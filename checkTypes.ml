
(*
  TypeMap is a map where the key is a string (the name of the variable)
  and the data is the type of the variable.
     
  The data is a _type list, because for functions and procedures we need
  to save the types of the arguments as well as the type of the function
*)
module TypeMap = Map.Make(String)

open TypeMap
open Ast

(*
  Exception raised when the expressions in the program does not
  match the expected patterns.
*)
exception TypeError of string

(**
  @brief [check_types_expr expr expectedType typeMap] checks if the type
    of the expression [expr] is the expected type. Returns true if it's
    the case, else false.
  
  @param expr : expression
  @param expectedType : _type
  @param typeMap : (_type list) TypeMap.t

  @return bool
*)
let rec check_types_expr expr expectedType typeMap = 
  match expr with
  |Int(_) -> expectedType = Integer
  |True |False -> expectedType = Boolean
  |Id(name) -> 
    (
      match find name typeMap with
      |actualType::_ -> expectedType = actualType
      |_ -> raise (TypeError "Wrong argument (check_types_expr).")
    )
  |Minus(expr') -> 
    (* The expression must be an Integer *)
    expectedType = Integer
    && check_types_expr expr' Integer typeMap
  |Comp(_,e1,e2) ->
    (* The result of a comparison must be a Boolean
       and the elements compared must be Integers *) 
    expectedType = Boolean
    && check_types_expr e1 Integer typeMap
    && check_types_expr e2 Integer typeMap
  |Op(_,e1,e2) ->
    (* Both of the expressions must be Integer, and the result too *)
    expectedType = Integer
    && check_types_expr e1 Integer typeMap
    && check_types_expr e2 Integer typeMap
  |Funct(name,exprList) ->
    (
      match find name typeMap with
      |funType::argsTypes ->
        (* Checking the function type and the arguments types *)
        List.fold_left2 
          (fun b t e -> b && check_types_expr e t typeMap)
          (expectedType = funType)
          argsTypes
          exprList

      |_->raise (TypeError "Wrong argument (check_types_expr).")
    )
  |Tab(Id(name),e) -> 
    (* the index of a tab must be an Integer *)
    check_types_expr e Integer typeMap
    && (* checking the type of the tab reference *)
    (
      match find name typeMap with
      |(Array(actualType))::_ -> actualType = expectedType
      |_-> raise (TypeError "Wrong argument (check_types_expr).") 
    )
  |Tab(Tab(e1,e2),e3) ->
    check_types_expr e3 Integer typeMap
    && check_types_expr (Tab(e1,e2)) (Array(expectedType)) typeMap
  |CreateTab(t,e) -> 
    check_types_expr e Integer typeMap
    && expectedType = Array(t)
  |_-> raise (TypeError "Wrong argument (check_types_expr).")


(**
  @brief [check_types_cond cond typeMap] checks if the elements
    of the conditions are well typed, and if the condition is
    a Boolean.
    Returns true if it's the case, else false.
      
  @param cond : condition
  @param typeMap : (_type list) TypeMap.t

  @return bool
*)
let rec check_types_cond cond typeMap = 
  match cond with
  | Expr(e) -> check_types_expr e Boolean typeMap 
  | Not(c) -> check_types_cond c typeMap
  | Or(c1,c2) -> 
    check_types_cond c1 typeMap
    && check_types_cond c2 typeMap
  | And(c1,c2) ->
    check_types_cond c1 typeMap
    && check_types_cond c2 typeMap


(**
  @brief [get_tab_type_aux tab typeMap] gets the type of the array element
  in argument. Useful in check_types_bloc. This function uses 2 auxiliary
  functions.
  
  @param tab : expression
  @param typeMap : (_type list) TypeMap.t

  @return _type
*)
(* gets the type of the id *)
let rec get_tab_type_aux1 tab typeMap = 
  match tab with
  |Tab(Id(name),_) -> 
    (match find name typeMap with
    |(Array(t)::_) -> Array(t)
    |_-> raise (TypeError "Wrong argument (get_tab_type)."))
  |Tab(Tab(e1,e2),_) -> 
    get_tab_type_aux1 (Tab(e1,e2)) typeMap
  |_-> raise (TypeError "Wrong argument (get_tab_type).")
;;
(* gets the type of the array element, using the type of the id *)
let rec get_tab_type_aux2 tab t =
    match tab with
    |Tab(Id(_),_) -> 
      (match t with |Array(t') -> t'
                    | _-> raise (TypeError "Wrong argument (get_tab_type.)") )
    |Tab(Tab(e1,e2),_) -> get_tab_type_aux2 (Tab(e1,e2)) 
        (match t with |Array(t') -> t'
                    | _-> raise (TypeError "Wrong argument (get_tab_type.)"))
  
    | _-> raise (TypeError "Wrong argument (get_tab_type).")
  ;; 
(* gets the type of the array element *)
let get_tab_type tab typeMap =
  let t = get_tab_type_aux1 tab typeMap in
  get_tab_type_aux2 tab t
;;



(**
  @brief [check_types_bloc instList typeMap] checks if all the instructions
  in the bloc are well typed. Returns true if it's the case, else false.
  
  @param instList : instruction list
  @param typeMap : (_type list) TypeMap.t

  @return bool
*)
let rec check_types_bloc instList typeMap = 
  match instList with
  | [] -> true
  | inst::instList' -> 
    (
      match inst with
      (* For write and writeln, we don't check the argument types *)
      |Proc("write",_) |Proc("writeln",_) -> 
        check_types_bloc instList' typeMap

      |Proc(name,exprList) ->
        (* getting the arguments types *)
        let argTypes = find name typeMap in
        List.fold_left2
          (fun b t e -> b && check_types_expr e t typeMap)
          true
          argTypes
          exprList
        && check_types_bloc instList' typeMap

      |VarAffectation(name,expr) ->
        (
          match find name typeMap with
          |varType::_ -> check_types_expr expr varType typeMap
          |_-> raise (TypeError "Wrong argument (check_types_bloc).")
        )
        && check_types_bloc instList' typeMap

      |TabAffectation(Id(name),expr1,expr2) -> 
        check_types_expr expr1 Integer typeMap
        && 
        (
          match find name typeMap with
          |(Array(t))::_ -> check_types_expr expr2 t typeMap
          |_::_ -> false
          |_-> raise (TypeError "Wrong argument (check_types_bloc).")
        )
        && check_types_bloc instList' typeMap

      |TabAffectation(Tab(e1,e2),e3,e4) -> 
        check_types_expr e3 Integer typeMap
        &&
        let t = get_tab_type (Tab(Tab(e1,e2),e3)) typeMap in
        check_types_expr e4 t typeMap
        && check_types_expr (Tab(Tab(e1,e2),e3)) t typeMap

      |Cond(c,inst1,inst2) ->
        check_types_cond c typeMap
        && check_types_bloc (inst1::inst2::instList') typeMap

      |Loop(c,inst') ->
        check_types_cond c typeMap
        && check_types_bloc (inst'::instList') typeMap

      |Bloc(instList'') ->
        check_types_bloc (instList'' @ instList') typeMap
      | _ -> raise (TypeError "Wrong argument (check_types_bloc).")
    )


(**
  @brief [check_types_funcProc funcProc typeMap] checks if the
    function/procedure in argument is well typed, ie. it checks if
    all the variables used in the body of the function are
    in the type in which they were declared. 

    Returns true if it's the case, else false.

  @param funcProc : funcProc
  @param typeMap : (_type list) TypeMap.t

  @return bool
*)
let check_types_funcProc funcProc typeMap = 
  match funcProc with
  |Function(_,env,_,envop,Bloc(instList))
  |Procedure(_,env,envop,Bloc(instList)) ->
    (
    (* Adding the arguments of the function in the typeMap *)
    let new_typeMap = 
      List.fold_left
      (
        fun tmap (nameList,t) ->
          List.fold_left
          (fun tmap' argName -> add argName [t] tmap')
          tmap
          nameList
      )
      typeMap
      env 
    in
    match envop with
    |None -> check_types_bloc instList new_typeMap
    |Some(localVars) -> 
      (* Adding the local variables' types to the map *)
      let new_typeMap' =
        List.fold_left
        (
        fun tmap (nameList,t) ->
          List.fold_left
          (fun tmap' argName -> add argName [t] tmap')
          tmap
          nameList
        )
        new_typeMap
        localVars
      in
      check_types_bloc instList new_typeMap' 
    )
  |_ -> raise (TypeError "Wrong argument (check_types_funcProc)")


(**
  @brief [addn n elt lst] is adding [n] times the element [elt]
  to the list [lst] and returns it
  
  Useful in the function [check_types_funcProcList]

  @param n : int, n>=0
  @param elt : _type
  @param lst : _type list

  @return _type list
*)
let rec addn n elt lst = 
  match n with
  |0 -> lst
  |_ -> addn (n-1) elt (elt::lst)
;;

(**
  @brief [check_types_funcProcList funcProcList typeMap] returns the
  couple (b,new_typeMap) where [b] is a boolean, true there is
  no type errors in the functions/procedures in the list ;
  [new_typeMap] is the type map where the functions and their
  arguments' types are added

  @param funcProcList : funcProc list
  @param typeMap : (_type list) TypeMap.t

  @return bool * TypeMap.t
*)
let check_types_funcProcList funcProcList typeMap =
  (* In the type map, we must include the types of all the functions
     and the types of the functions and procedures' arguments,
     because they can be used in other functions/procedures implementation.

     The _type list linked to the name of a function is of this structure:
     [functionType ; arg1Type ; ... ; argnType]
     For a procedure :
     [arg1Type ; ... ; argnType] *)
  let new_typeMap =
    List.fold_left
    (fun tmap fp ->
      match fp with
      |Function(name,args,t,_,_)->
        add
        name
        (t::
          (
            (*
              Tranform the environment list in _type list of the 
              arguments in the correct order.
              Example: 
              args = [(["x";"y"],Integer);(["o"],Boolean);(["t";"u"],Integer)]
              result -> [Integer;Integer;Boolean;Integer;Integer]
   
            *)
            List.fold_right
            (fun (nameList,argType) typeList ->
              addn (List.length nameList) argType typeList)
            args
            []
          )
        )
        tmap
      
      (*It is the same thing for the procedure except that 
         the first type of the list isn't the type of
         the function but the type of the first argument*)
      |Procedure(name,args,_,_) -> 
        add
        name
        (
          (*
            Tranforming the environment in _type list of the 
            arguments in the correct order.
            Example: 
            args = [(["x";"y"],Integer);(["o"],Boolean);(["t";"u"],Integer)]
            result -> [Integer;Integer;Boolean;Integer;Integer]
          *)
          List.fold_right
          (fun (nameList,argType) typeList ->
            addn (List.length nameList) argType typeList)
          args
          []
        )
        tmap
    )
    typeMap
    funcProcList
  in
  (* Now we can get the result using the new typeMap *)
  let res =
  (* checking if all the functions/procedures are well typed *)
  List.fold_left
  (
    fun b funcProc -> 
      (b && check_types_funcProc funcProc new_typeMap)
  )
  true
  funcProcList
  
  in
  (* Returning the result and the new typeMap*)
  (res,new_typeMap)
  

(**
  @brief [check_types prog] checks if all elements in the program
    [prog] are well typed. Returns true if it's the case, else false.
    It can also raise TypeError(msg), which means that the program
    [prog] is not well typed.
  
  @param prog : program

  @return bool
*)
let check_types prog = 
  (*adding readln in the map*)
  let typeMap = add "readln" [Integer] empty 
  in
  match prog with

  |None,None,Bloc(instList) ->
    check_types_bloc instList typeMap

  |None,Some(funcProcList),Bloc(instList) -> 
    let (b,new_typeMap) = check_types_funcProcList funcProcList typeMap
    in b && check_types_bloc instList new_typeMap

  |Some(nameTypeList),None,Bloc(instList) ->
    (* adding the variables to the type map *)
    let new_typeMap = 
      List.fold_left
        (
        fun tmap (nameList,t) -> 
          List.fold_left
            (fun tmap' name -> add name [t] tmap')
            tmap
            nameList 
        )
        typeMap
        nameTypeList
    in
    check_types_bloc instList new_typeMap

  |Some(nameTypeList),Some(funcProcList),Bloc(instList) ->
    (* adding the variables to the type map *)
    let new_typeMap = 
      List.fold_left
        (
        fun tmap (nameList,t) -> 
          List.fold_left
            (fun tmap' name -> add name [t] tmap')
            tmap
            nameList 
        )
        typeMap
        nameTypeList
    in      
    let (b,new_typeMap) = check_types_funcProcList funcProcList new_typeMap
    in b && check_types_bloc instList new_typeMap

  | _ -> raise (TypeError "Wrong argument (check_types).")
