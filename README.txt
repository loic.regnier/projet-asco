Compilation :
Type "make" and then follow the instructions.

How to use the generated executables?
Start the executable with at least 1 argument, the argument(s) are the Pseudo-Pascal
file(s) that you want to analyse.

Examples:
./printAST projet_pp_synsem/OK/a.p
./main projet_pp_synsem/OK/*

NB: the AST can not be printed by printAST if there is a syntax error.