(*
   Exception raised when the expressions in the program does not
   match the expected patterns.
*)
exception TypeError of string

(**
  @brief [check_types prog] checks if all elements in the program
    [prog] are well typed. Returns true if it's the case, else false.
    It can also raise TypeError(msg), which means that the program
    [prog] is not well typed.
  
  @param prog : program

  @return bool
*)
val check_types : Ast.program -> bool