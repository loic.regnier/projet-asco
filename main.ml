(*
  [main.ml] is parsing the pseudo pascal files in argument of the exectuable,
  and is checking if the code has no scope errors, if it's the case, it checks if it's correctly
  typed aswell.
*)

let _ = 
(* number of files to analyse *)
let n = Array.length Sys.argv in

if n < 2 then print_endline 
  "main : Error : At least 1 argument is expected."

else
  let ext = ref 0 in (* Exit value *)
  let scope = ref false in (* = true if the scope is correct, else false *)
  
  (* Analysing all the files *)
  for k=1 to (n-1) do
    (* Opening the kth file *)
    let filename = Sys.argv.(k) in
    let file = open_in filename in
    let lexbuf = Lexing.from_channel file in
    print_string (filename^" :\n");
(
  try
      (* reading the file *)
      (let res = Parser.prog Lexer.token lexbuf in
      (* checking the potential scope errors *)
      scope := CheckScope.check_scope res ;
      if !scope
      then (print_string "  Correct scope.\n" ;
          if CheckTypes.check_types res 
            then print_string "  Well typed.\n"
            else print_string "  Error : Not well typed.\n")
      else print_string "  Error : Incorrect scope.\n");
      print_string "\n"
  with
  (* for all the exceptions/errors, the exit value is 1 *)
    |Parsing.Parse_error -> (* syntax error *)
     let open Lexing in
     let sp = lexeme_start_p lexbuf in
     let ep = lexeme_end_p lexbuf in
     let starting_char = sp.pos_cnum - sp.pos_bol in
     let ending_char = ep.pos_cnum - ep.pos_bol in
     Printf.printf "  Syntax error line %d, characters %d to %d:\n\n"
       sp.pos_lnum starting_char ending_char;
       seek_in file (sp.pos_bol) ;
       let indent = ((string_of_int sp.pos_lnum)^" | ") in
       Printf.printf "%s%s\n" indent (input_line file);
       for i=0 to (starting_char -1) do
        Printf.printf " ";
       done;
       for i=0 to (String.length indent -1) do
        Printf.printf " ";
       done;
       for i=0 to (ending_char - starting_char -1) do
        Printf.printf "^";
       done; Printf.printf "\n";
       ext := 1
      
    |CheckScope.ScopeError msg -> (* scope error *)
      Printf.printf "  Error : Incorrect scope \"%s\"\n\n" msg ; ext := 1

    |CheckTypes.TypeError msg -> (* type error *)
      Printf.printf "  Error : Not well typed \"%s\"\n\n" msg ; ext := 1

    | _ -> (* Other exceptions might be raised, in this case it's either
       a scope or type error.*)
          if !scope then Printf.printf "  Error : Incorrect scope.\n\n" 
          else Printf.printf "  Error : Not well typed.\n\n" ; 
    ext := 1

) ;
(* closing the file *)
close_in file

done; 
(* all the files are analysed *)

(* exiting with the value !ext (=0 if there were no errors, else =1)*)
exit !ext