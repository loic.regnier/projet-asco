all: usage

usage: 
	@echo "Type \"make lexpar\" first.\nThen \"make main\" or \"make printAST\"." 

lexpar: lexer parser

lexer: lexer.mll
	ocamllex $<

parser: parser.mly
	ocamlyacc $<

%.cmi: %.mli
	ocamlc -c $<

%.cmo: %.ml
	ocamlc -c $<

main: ast.cmo parser.cmo lexer.cmo checkScope.cmo checkTypes.cmo main.cmo
	ocamlc -o $@ $^

printAST: ast.cmo parser.cmo lexer.cmo printAST.cmo
	ocamlc -o $@ $^

clean:
	rm lexer.ml parser.ml parser.mli *.cm[io] main printAST
	
depend:
	ocamldep *.mli *.ml > .depend

include .depend
