(*
   Exception raised when the expressions in the program does not
   match the expected patterns.
*)
exception ScopeError of string

(**
  @brief [check_scope prog] checks if the program [prog] is correctly
    scoped. Returns true if it's the case, else false.
  
  @param prog : program

  @return bool
*)
val check_scope : Ast.program -> bool