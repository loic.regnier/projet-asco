(*
  [printAST] enable the possibility to visualize the Abstract Syntax Tree.
  The process is the same as [main.ml], it prints the AST of all the
  pseudo pascal files in arguments. Of course, it can't print the tree if 
  there is a syntax error.

  The result is just informative and not made to be good-looking. It becomes
  difficult to read when the code is long.

  It's useful to know if the resulting AST is the one expected.
*)

open Ast
open Printf

(* type_to_string : _type -> string *)
let rec type_to_string t = match t with
| Integer -> "Integer"
| Boolean -> "Boolean"
| Array(t') -> "Array("^(type_to_string t')^")"

(* op_to_string : op -> string *)
let op_to_string o = match o with
| Add -> "Add"
| Sub -> "Sub"
| Mul -> "Mul"
| Div -> "Div"

(* comp_to_string : comp -> string *)
let comp_to_string c = match c with
| Eq -> "Eq"
| Leq -> "Leq"
| Geq -> "Geq"
| Lt -> "Lt"
| Gt -> "Gt"
| Neq -> "Neq"

(* print_expr : expression -> unit*)
let rec print_expr e = match e with
| Int i -> printf "Int(%i)" i
| True -> printf "true"
| False -> printf "false"
| Id s -> printf "Id(%s)" s
| Minus e' -> printf "Minus("; print_expr e'; printf ")"
| Comp (c,e1,e2) -> printf "%s" ("Comp("^(comp_to_string c)^","); print_expr e1 ; printf ","; print_expr e2; printf ")"
| Op (o,e1,e2) -> printf "%s" ("Op("^(op_to_string o)^","); print_expr e1 ; printf ","; print_expr e2; printf ")"
| Funct (s,el) ->(
  let rec print_fun_arg al = match al with
| [] -> ()
| [a] -> print_expr a
| a::al' -> (print_expr a ; printf ","; print_fun_arg al' )
in
  printf "Funct(%s," s; print_fun_arg el; printf ")")
| Tab (e1,e2) -> printf "Tab(";print_expr e1; printf ","; print_expr e2; printf ")"
| CreateTab (t,e) -> printf "%s" ("NewTab("^(type_to_string t)^","); 
print_expr e ; printf ")"
;;



let rec print_string_list sl = match sl with
| [] -> ()
| [a] -> print_string a
| a::al' -> print_string a ; printf ","; print_string_list al' 
;; 
(*  ((string list) * _type) list *)
let rec print_env l = match l with
| [] -> ()
| [sl,t] -> printf "Var("; print_string_list sl ; printf "%s"(","^(type_to_string t)^")") 
| (sl,t)::l' -> printf "Var("; print_string_list sl ; printf "%s" (","^(type_to_string t)^")");
printf " | "; print_env l'
;;

let print_envop envop = match envop with
| None -> printf "None"
| Some(e) -> print_env e
;;


let rec print_cond c = match c with
    Expr(e) -> printf "Expr("; print_expr e ; printf ")"
  | Not(c) -> printf "Not("; print_cond c; printf ")"
  | Or(c1,c2) -> printf "Or("; print_cond c1; printf ","; print_cond c2; printf ")"
  | And(c1,c2) -> printf "And("; print_cond c1; printf ","; print_cond c2; printf ")"
;;


let rec print_inst i = match i with
| Proc (s,explst) -> (
  let rec print_fun_arg al = match al with
  | [] -> ()
  | [a] -> print_expr a
  | a::al' -> (print_expr a ; printf ","; print_fun_arg al' )
  in  
printf "Proc(%s," s; print_fun_arg explst; printf ")")
| VarAffectation (s,e) -> printf "VarAff(%s," s; print_expr e; printf ")"
| TabAffectation (e1,e2,e3) -> printf "TabAff("; print_expr e1; printf ","; print_expr e2; printf ","; print_expr e3; printf ")"
| Cond(c,i1,i2) -> printf "Cond("; print_cond c ; printf ","; print_inst i1; printf ","; print_inst i2; printf ")"
| Loop (c,i) -> printf "Loop("; print_cond c; printf ","; print_inst i; printf ")"
| Bloc(ilst) -> (
let rec print_inst_list instlist = match instlist with
|[] -> ()
|[i] -> print_inst i
|i::instlist' -> (print_inst i ; printf ","; print_inst_list instlist') in  
printf "Bloc("; print_inst_list ilst; printf ")")
;;

let print_funcproc f = match f with
| Function (s,env,t,envop,i) ->
  printf "Fun("; print_string s; printf ","; print_env env; 
  printf "%s" (","^(type_to_string t)^","); print_envop envop; printf ","; print_inst i ; printf ")" 
| Procedure (s,env,envop,i) -> 
  printf "Proc("; print_string s; printf ","; print_env env; printf ",";
  print_envop envop; printf ","; print_inst i ; printf ")";
;;



let rec print_funcproc_list l = match l with
[] -> ()
|[f] -> print_funcproc f
|f::l' -> print_funcproc f; printf "\n"; print_funcproc_list l'
;;

let print_funcproc_list_op lop = match lop with
None -> printf "None"
| Some(fpl) -> print_funcproc_list fpl
;;

(*    environment option (* global variables definitions *)
  * (funcProc list) option (* function and procedure definitions *)
  * instruction*)
let print_prog p = let envop,fplop,i = p in
printf "\nAbstract Syntax Tree :\n\n" ;
printf "Program\n(\n"; print_envop envop; printf ",\n\n";
print_funcproc_list_op fplop; printf ",\n\n";
print_inst i; printf "\n)\n"
;;


let _ = 
  let n = Array.length Sys.argv in
  
  if n < 2 then print_endline 
    "main : Error : At least 1 argument is expected."
  
  else
    for k=1 to (n-1) do
      (* Opening the kth file *)
      let filename = Sys.argv.(k) in
      let file = open_in filename in
      let lexbuf = Lexing.from_channel file in
  (
    try
        let res = Parser.prog Lexer.token lexbuf in
        print_string ("----------------------------|"^filename^"|----------------------------\n");
        print_prog res ;
    with
    |Parsing.Parse_error ->
      let open Lexing in
      let sp = lexeme_start_p lexbuf in
      let ep = lexeme_end_p lexbuf in
      let starting_char = sp.pos_cnum - sp.pos_bol in
      let ending_char = ep.pos_cnum - ep.pos_bol in
      Printf.printf "  Syntax error line %d, characters %d to %d:\n\n"
        sp.pos_lnum starting_char ending_char;
        seek_in file (sp.pos_bol) ;
        let indent = ((string_of_int sp.pos_lnum)^" | ") in
        Printf.printf "%s%s\n" indent (input_line file);
        for i=0 to (starting_char -1) do
         Printf.printf " ";
        done;
        for i=0 to (String.length indent -1) do
         Printf.printf " ";
        done;
        for i=0 to (ending_char - starting_char -1) do
         Printf.printf "^";
        done; Printf.printf "\n";
       exit 1
  
  ) ;
   close_in file
  
    done