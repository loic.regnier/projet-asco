{
  open Parser
}
let digit = ['0'-'9']
let alphau = ['a'-'z''A'-'Z''_']
              
rule token = parse
   | '+' { PLUS }
   | '-' { MINUS }
   | '*' { TIMES }
   | '/' { DIV }
   | '(' { LPAR }
   | ')' { RPAR }
   | '[' { LSQBRACKET }
   | ']' { RSQBRACKET }
   | ":=" { AFFECT}
   | '=' { EQ }
   | "<=" { LEQ }
   | ">=" { GEQ }
   | "<>" { NEQ }
   | '<' { LT }
   | '>' { GT }
   | ';' { SEMICOL}
   | ',' { COMA }
   | ':' { COL }
   | '.' { DOT }
   | '\n' { Lexing.new_line lexbuf ; token lexbuf (* new line *)}
   | [' ''\t']+ { token lexbuf (* skip spaces *) }
   | '{' [^'}']* '}'  { token lexbuf (* skip comments *) }
   | digit+ as s { INT (int_of_string s) }
   | "var" { VAR }
   | "integer" { INTEGER }
   | "boolean" { BOOLEAN }
   | "not" { NOT }
   | "or" { OR }
   | "and" { AND }
   | "new" { NEW }
   | "array" { ARRAY }
   | "function" { FUNCTION }
   | "procedure" { PROCEDURE }
   | "program" { PROGRAM }
   | "begin" { BEGIN }
   | "end" { END }
   | "of" { OF }
   | "true" { TRUE }
   | "false" { FALSE }
   | "if" { IF }
   | "then" { THEN }
   | "else" { ELSE }
   | "while" { WHILE }
   | "do" { DO }
   | "repeat" { REPEAT }
   | "until" { UNTIL }
   | alphau(alphau|digit)* as s { ID s }
   