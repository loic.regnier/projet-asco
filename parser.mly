%{
  open Ast
%}
%token PLUS MINUS TIMES DIV NOT OR AND
%token LPAR RPAR LSQBRACKET RSQBRACKET
%token EQ LEQ GEQ NEQ LT GT 
%token SEMICOL COMA COL DOT 
%token VAR NEW AFFECT FUNCTION PROCEDURE 
%token INTEGER BOOLEAN ARRAY OF
%token PROGRAM BEGIN END 
%token TRUE FALSE IF THEN ELSE WHILE DO REPEAT UNTIL
%token<int> INT
%token<string> ID
%left EQ LEQ GEQ NEQ LT GT
%left PLUS MINUS OR 
%left TIMES DIV AND
%nonassoc UMINUS NOT
%start prog
%type<Ast.program> prog
%type<Ast._type> type
%type<Ast.expression> expression
%type<Ast.condition> condition
%type<Ast.instruction> instruction
%type<Ast.environment> environment
%type<Ast.funcProc list> defFuncProc
%%

prog: 
  | PROGRAM defVar defFuncProc BEGIN instructions END DOT
    {Some($2),Some($3),Bloc($5)}
  | PROGRAM defVar BEGIN instructions END DOT
    {Some($2),None,Bloc($4)}
  | PROGRAM defFuncProc BEGIN instructions END DOT
    {None,Some($2),Bloc($4)}
  | PROGRAM BEGIN instructions END DOT
    {None,None,Bloc($3)}

ids: 
  | ID { [$1] }
  | ID COMA ids { $1::$3 } 

type:
    INTEGER { Integer }
  | BOOLEAN { Boolean }
  | ARRAY OF type { Array($3)}

defVar:
    VAR defVars {$2}

defVars:
  | ids COL type SEMICOL defVars {($1,$3)::$5}
  | ids COL type SEMICOL {[$1,$3]}

defFuncProc:
  | defFuncs {$1}
  | defProcs {$1}

defFuncs:
  | FUNCTION ID LPAR environment RPAR COL type SEMICOL
    defVar BEGIN instructions END SEMICOL
    {[Function($2,$4,$7,Some($9),Bloc($11))]}
  | FUNCTION ID LPAR environment RPAR COL type SEMICOL
    BEGIN instructions END SEMICOL
    {[Function($2,$4,$7,None,Bloc($10))]}
  | FUNCTION ID LPAR environment RPAR COL type SEMICOL
    defVar BEGIN instructions END SEMICOL defFuncs
    {(Function($2,$4,$7,Some($9),Bloc($11)))::$14}
  | FUNCTION ID LPAR environment RPAR COL type SEMICOL
    BEGIN instructions END SEMICOL defFuncs
    {(Function($2,$4,$7,None,Bloc($10)))::$13}
  | FUNCTION ID LPAR environment RPAR COL type SEMICOL
    defVar BEGIN instructions END SEMICOL defProcs
    {(Function($2,$4,$7,Some($9),Bloc($11)))::$14}
  | FUNCTION ID LPAR environment RPAR COL type SEMICOL
    BEGIN instructions END SEMICOL defProcs
    {(Function($2,$4,$7,None,Bloc($10)))::$13}

defProcs:
  | PROCEDURE ID LPAR environment RPAR SEMICOL
    defVar BEGIN instructions END SEMICOL
    {[Procedure($2,$4,Some($7),Bloc($9))]}
  | PROCEDURE ID LPAR environment RPAR SEMICOL
    BEGIN instructions END SEMICOL
    {[Procedure($2,$4,None,Bloc($8))]}
  | PROCEDURE ID LPAR environment RPAR SEMICOL
    defVar BEGIN instructions END SEMICOL defProcs
    {(Procedure($2,$4,Some($7),Bloc($9)))::$12}
  | PROCEDURE ID LPAR environment RPAR SEMICOL
    BEGIN instructions END SEMICOL defProcs
    {(Procedure($2,$4,None,Bloc($8)))::$11}
  | PROCEDURE ID LPAR environment RPAR SEMICOL
    defVar BEGIN instructions END SEMICOL defFuncs
    {(Procedure($2,$4,Some($7),Bloc($9)))::$12}
  | PROCEDURE ID LPAR environment RPAR SEMICOL
    BEGIN instructions END SEMICOL defFuncs
    {(Procedure($2,$4,None,Bloc($8)))::$11}

instruction:
  | ID LPAR expressions RPAR {Proc($1,$3)}
  | ID AFFECT expression {VarAffectation($1,$3)}
  | expression LSQBRACKET expression RSQBRACKET AFFECT expression
    {TabAffectation($1,$3,$6)}
  | IF condition THEN instruction ELSE instruction
    {Cond($2,$4,$6)}
  | WHILE condition DO instruction 
    {Loop($2,$4)}
  | BEGIN instructions END 
    {Bloc($2)}
  | REPEAT instructions UNTIL condition 
    {Bloc($2 @ [Loop($4,Bloc($2))])}

instructions:
  | instruction {[$1]}
  | instruction SEMICOL instructions {$1::$3}
  | {[]}

condition:
  | expression {Expr($1)}
  | NOT condition {Not($2)}
  | condition AND condition {And($1,$3)}
  | condition OR condition {Or($1,$3)}
  | LPAR condition RPAR {$2}

expression:
  | INT {Int($1)}
  | TRUE {True}
  | FALSE {False}
  | ID {Id($1)}
  | MINUS expression %prec UMINUS {Minus($2)}
  | LPAR expression RPAR {$2}
  | expression TIMES expression {Op(Mul,$1,$3)}
  | expression PLUS expression {Op(Add,$1,$3)}
  | expression MINUS expression {Op(Sub,$1,$3)}
  | expression DIV expression {Op(Div,$1,$3)}
  | expression EQ expression {Comp(Eq,$1,$3)}
  | expression LEQ expression {Comp(Leq,$1,$3)}
  | expression GEQ expression {Comp(Geq,$1,$3)}
  | expression LT expression {Comp(Lt,$1,$3)}
  | expression GT expression {Comp(Gt,$1,$3)}
  | expression NEQ expression {Comp(Neq,$1,$3)}
  | ID LPAR expressions RPAR {Funct($1,$3)}
  | expression LSQBRACKET expression RSQBRACKET
    {Tab($1,$3)}
  | NEW ARRAY OF type LSQBRACKET expression RSQBRACKET
    {CreateTab($4,$6)}

expressions:
  | expression COMA expressions {$1::$3}
  | expression {[$1]}
  | {[]}

environment:
  | ids COL type SEMICOL environment {($1,$3)::$5}
  | ids COL type {[$1,$3]}
  | {[]}


