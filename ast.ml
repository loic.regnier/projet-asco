
type _type =
    Integer
  | Boolean
  | Array of _type

type op =
    Add
  | Sub
  | Mul
  | Div

type comp =
  | Eq
  | Leq
  | Geq
  | Lt
  | Gt
  | Neq

type expression = 
    Int of int
  | True
  | False
  | Id of string
  | Minus of expression
  | Comp of comp * expression * expression
  | Op of op * expression * expression
  | Funct of string * (expression list)
  | Tab of expression * expression
  | CreateTab of _type * expression

type condition =
    Expr of expression
  | Not of condition
  | Or of condition * condition
  | And of condition * condition


type instruction =
    Proc of string * (expression list)
  | VarAffectation of string * expression
  | TabAffectation of expression * expression * expression
  | Cond of condition * instruction * instruction
  | Loop of condition * instruction
  | Bloc of instruction list


type environment = 
    ((string list) * _type) list


type funcProc = 
    Function of string * environment * _type * (environment option) * instruction
  | Procedure of string * environment * (environment option) * instruction

type program = 
    environment option (* global variables definitions *)
  * (funcProc list) option (* function and procedure definitions *)
  * instruction