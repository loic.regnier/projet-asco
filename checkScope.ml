open Ast

(*
   Exception raised when the expressions in the program does not
   match the expected patterns.
*)
exception ScopeError of string


(**
  @brief [check_scope expr var_env] checks if the expression [expr] has no
  scope errors. Returns true if it's the case, else false.
    
  @param expr : expression
  @param var_env : string list

  @return bool
*)
let rec check_scope_expr expr var_env = 
  match expr with
  |Int(_) |True |False -> true (* integer or boolean *)

  |Id(name) -> List.mem name var_env (* checking if the variable is declared *)
  
  |Minus(expr') -> check_scope_expr expr' var_env (* checking the expression *)
  
  |Comp(_,e1,e2) -> (* checking both expression *)
    check_scope_expr e1 var_env
    && check_scope_expr e2 var_env
  
  |Op(_,e1,e2) -> (* checking both expression *)
    check_scope_expr e1 var_env
    && check_scope_expr e2 var_env
  
  |Funct(name,exprList) ->
    (* checking if the function is declared *)
    List.mem name var_env
    (* checking all the expressions in the list *)
    && List.fold_left
      (fun acc e -> acc && check_scope_expr e var_env)
      true
      exprList
  
  |Tab(Id(name),e) -> (*checking if the array is declared *)
    List.mem name var_env
    (* checking the expression *)
    && check_scope_expr e var_env
  
  |Tab(Tab(e1,e2),e3) -> (* checking if the array is declared *)
    check_scope_expr (Tab(e1,e2)) var_env
    (* checking the expression *)
    && check_scope_expr e3 var_env
  
  |CreateTab(_,e) -> check_scope_expr e var_env (*checking the expression*)
  
  |_-> raise (ScopeError "Wrong argument (check_scope_expr).")


(**
  @brief [check_scope_cond cond var_env] checks if the condition [cond] has
  no scope errors. Return true if it's the case, else false.
    
  @param cond : condition
  @param var_env : string list

  @return bool
*)
let rec check_scope_cond cond var_env = 
  match cond with
  |Expr(e) -> (* checking the scope of the expression *)
    check_scope_expr e var_env
  |Not(c) -> (* checking the condition *)
    check_scope_cond c var_env
  |Or(c1,c2) -> (* checking both conditions *)
    check_scope_cond c1 var_env
    && check_scope_cond c2 var_env
  |And(c1,c2) -> (* checking both conditions *)
    check_scope_cond c1 var_env
    && check_scope_cond c2 var_env


(**
  @brief [check_scope_bloc instList var_env] checks if all the instructions
  in the list [instList] has no scope erros. Returns true if it's the case,
  else false.
  
  @param instList : instruction list
  @param var_env : string list

  @return bool
*)
let rec check_scope_bloc instList var_env = 
  match instList with
  | [] -> true (* empty bloc *)
  | inst::instList' -> 
    (
      match inst with
      |Proc(name,exprList) -> 
        (* checking if the procedure is declared *)
        List.mem name var_env
        (* checking all the expressions in the list *)
        && List.fold_left
          (fun acc e -> acc && check_scope_expr e var_env)
          true
          exprList
        (* checking the rest of the instructions *)
        && check_scope_bloc instList' var_env
      
      |VarAffectation(name,expr) ->
        (* checking if the variable is declared *)
        List.mem name var_env
        (* checking the scope of the expression *)
        && check_scope_expr expr var_env
        (* checking the rest of the instructions*)
        && check_scope_bloc instList' var_env

      |TabAffectation(Id(name),expr1,expr2) ->
        (* checking if the array is declared *)
        List.mem name var_env
        (* checking the scope of both expressions *)
        && check_scope_expr expr1 var_env
        && check_scope_expr expr2 var_env

      |TabAffectation(Tab(e1,e2),e3,e4) ->
        (* checking the scope of the array *)
        check_scope_expr (Tab(e1,e2)) var_env
        (* checking the scope of both expressions *)
        && check_scope_expr e3 var_env
        && check_scope_expr e4 var_env

      |Cond(c,inst1,inst2) ->
        (* checking the condition *)
        check_scope_cond c var_env
        (* checking both of instructions with the rest *)
        && check_scope_bloc (inst1::inst2::instList') var_env

      |Loop(c,inst') ->
        (* checking the condition *)
        check_scope_cond c var_env
        (* checking the instruction with the rest *)
        && check_scope_bloc (inst'::instList') var_env

      |Bloc(instList'') ->
        (* if it's a bloc, we considere a bigger bloc with all the instructions *)
        check_scope_bloc (instList'' @ instList') var_env

      | _ -> raise (ScopeError "Wrong argument (check_scope_bloc).")
    )


(**
  @brief [check_scope_funcProc funcProc var_env] checks if the function
  (or procedure) [funcProc] has a correct scope. Returns true if it's the case,
  else false.

  @param funcProc : funcProc
  @param var_env : string list

  @return bool
*)
let check_scope_funcProc funcProc var_env = 
  match funcProc with
  |Function(name,env,_,envop,Bloc(instList))
  |Procedure(name,env,envop,Bloc(instList)) ->
    (
    (* Adding the arguments in the environment *)
    let argList = 
      List.concat 
      (
      List.map 
      (fun (nameList,_) -> nameList)
      env
      )
    in
    (* adding the name of the function to the environment *)
    let new_env = name::(argList @ var_env) 
    in
    (* adding the potential local variables *)
    match envop with
    |None -> (* no local variables so we check directly the body of the function *)
      check_scope_bloc instList new_env
    |Some(localVars) -> 
      (* Adding the local variables in the environment*)
      let localVarList =
        List.concat
        (
        List.map
        (fun (nameList,_) -> nameList)
        localVars
        )
      in
      (* checking the body with the new environment *)
      check_scope_bloc instList (localVarList @ new_env) 
    )
  |_ -> raise (ScopeError "Wrong argument (check_scope_funcProc)")


(** 
  @brief [check_scope_funcProcList funcProcList var_env] returns the pair
  (b,new_var_env) where [b] is a boolean, true if all the functions/procedures
  have no scope errors, else false; and [new_var_env] is the variable
  environment with the functions declared added.

  @param funcProcList : funcProc list
  @param var_env : string list

  @return bool * string list
*)
let check_scope_funcProcList funcProcList var_env =
  (* adding all the functions and procedures to the environment,
     because they can be used in other function/procedure bodies *)
  let new_env =
    var_env
    @
    List.map
    (fun fp ->
      match fp with
      |Function(name,_,_,_,_)
      |Procedure(name,_,_,_) -> name
    )
    funcProcList
  in
  (* calculating the result with the new variable environment *)
  let res =
  (* checking the scope of all the functions/procedures *)
  List.fold_left
  (
    fun b funcProc -> 
      (b && check_scope_funcProc funcProc new_env)
  )
  true
  funcProcList
  
  in
  (* returning the result and the new variable environment *)
  (res,new_env)
  

(**
  @brief [check_scope prog] checks if all elements in the program [prog]
    has no scope errors, ie. if each variables and functions are declared
    before being used. 
  
  @param prog : program

  @return bool
*)
let check_scope prog = 
  (* adding write, writeln and readln to the global environment *)
  let env = ["write";"writeln";"readln"] in
  match prog with

  |None,None,Bloc(instList) ->
    (* checking the instructions *)
    check_scope_bloc instList env

  |None,Some(funcProcList),Bloc(instList) -> 
    (* getting the environment with the functions and procedures *)
    let b,new_env = check_scope_funcProcList funcProcList env
    (* checking the instructions with this new environment *)
    in b && check_scope_bloc instList new_env

  |Some(nameTypeList),None,Bloc(instList) ->
    (* adding the global variables to the environment *)
    let globalVars = 
      List.concat
      (
      List.map 
        (fun (nameList,_) -> nameList)
        nameTypeList
      )
    in
    (* checking the instructions *)
    check_scope_bloc instList (env @ globalVars)

  |Some(nameTypeList),Some(funcProcList),Bloc(instList) ->
    (* adding the global variables to the environment *)
    let globalVars = 
    List.concat
    (
    List.map 
      (fun (nameList,_) -> nameList)
      nameTypeList
    )
    in
    (* getting the environment with the functions and procedures *)
    let (b,new_env) = check_scope_funcProcList funcProcList (env @ globalVars)
    (* checking the instructions *)
    in b && check_scope_bloc instList new_env 

  | _ -> (* an abstract syntax tree must be of one of the above patterns *)
    raise (ScopeError "Wrong argument (check_scope).")